//Axel Rivas Guillen - 329830

// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
// R: completado

// 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
// R: 800

// 3) Encuentra todas las calificaciones del estudiante con el id numero 4.
// R: > db.grades.find({"student_id":4});
//   "exam", "score" : 87.89071881934647 }
//   "homework", "score" : 28.656451042441 }
//   "quiz", "score" : 27.29006335059361 }
//   "homework", "score" : 5.244452510818443 }

// 4) ¿Cuántos registros hay de tipo exam?
// R: > db.grades.find({"type":"exam"}).count();
//    200

// 5) ¿Cuántos registros hay de tipo homework?
// R: > db.grades.find({"type":"homework"}).count();
//    400

// 6) ¿Cuántos registros hay de tipo quiz?
//> db.grades.find({"type":"quiz"}).count();
//    200


// 7) Elimina todas las calificaciones del estudiante con el id numero 3
//> db.grades.update({"student_id": 3, "type": "exam"}, { $unset:{ score:""} } );
//  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 0 })
//> db.grades.update({"student_id": 3, "type": "homework"}, { $unset:{ score:""} } );
//  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
//> db.grades.update({"student_id": 3, "type": "quiz"}, { $unset:{ score:""} } );
//  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
//> db.grades.update({"student_id": 3, "type": "homework"}, { $unset:{ score:""} } );
//  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 0 })
//> db.grades.update({"student_id": 3, "type": "homework", "score": {$exists: true}}, { $unset:{ score:""} } );
//WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
// > db.grades.find({"student_id": 3});
// { "_id" : ObjectId("50906d7fa3c412bb040eb583"), "student_id" : 3, "type" : "exam" }
// { "_id" : ObjectId("50906d7fa3c412bb040eb585"), "student_id" : 3, "type" : "homework" }
// { "_id" : ObjectId("50906d7fa3c412bb040eb584"), "student_id" : 3, "type" : "quiz" }
// { "_id" : ObjectId("50906d7fa3c412bb040eb586"), "student_id" : 3, "type" : "homework" }


// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
//> db.grades.find({"score": 75.29561445722392});
//  { "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
//> db.grades.update({"_id": ObjectId("50906d7fa3c412bb040eb591") }, {$set:{ "score" : 100 }});
//  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

// 10) A qué estudiante pertenece esta calificación.
//> db.grades.find({"_id": ObjectId("50906d7fa3c412bb040eb591")});
//  { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
